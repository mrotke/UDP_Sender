# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Sender.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!
import PyQt4

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Sender(object):
    def setupUi(self, Sender):
        Sender.setObjectName(_fromUtf8("Sender"))
        Sender.resize(636, 369)
        self.centralwidget = QtGui.QWidget(Sender)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.Sendline = QtGui.QLineEdit(self.centralwidget)
        self.Sendline.setObjectName(_fromUtf8("Sendline"))
        self.horizontalLayout_2.addWidget(self.Sendline)
        self.SendButton = QtGui.QPushButton(self.centralwidget)
        self.SendButton.setObjectName(_fromUtf8("SendButton"))
        self.horizontalLayout_2.addWidget(self.SendButton)
        self.horizontalLayout.addLayout(self.horizontalLayout_2)
        Sender.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(Sender)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 636, 23))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        Sender.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(Sender)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        Sender.setStatusBar(self.statusbar)
        self.toolBar = QtGui.QToolBar(Sender)
        self.toolBar.setObjectName(_fromUtf8("toolBar"))
        Sender.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)

        self.retranslateUi(Sender)
        QtCore.QMetaObject.connectSlotsByName(Sender)

    def retranslateUi(self, Sender):
        Sender.setWindowTitle(_translate("Sender", "Sender", None))
        self.SendButton.setText(_translate("Sender", "Send", None))
        self.toolBar.setWindowTitle(_translate("Sender", "toolBar", None))
    


