import socket
import sys

from PyQt4 import QtCore, QtGui

from SenderGUI import Ui_Sender


UDP_IP = "127.0.0.1"
UDP_PORT = 5005
 
sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_Sender()
        self.ui.setupUi(self)
        self.ui.SendButton.clicked.connect(self.sendBtnClicked)
    
    def sendBtnClicked(self):
        tekst = str(self.ui.Sendline.text())
        sock.sendto(tekst, (UDP_IP, UDP_PORT)) 


def run():
    app = QtGui.QApplication(sys.argv)
    myapp = MyForm()
    myapp.show()
    sys.exit(app.exec_())

    
run()    

# UDP_IP = "127.0.0.1"
# UDP_PORT = 5005
# MESSAGE = "Hello, World!"
# 
# print "UDP target IP:", UDP_IP
# print "UDP target port:", UDP_PORT
# print "message:", MESSAGE
# 
# sock = socket.socket(socket.AF_INET, # Internet
#                      socket.SOCK_DGRAM) # UDP
# sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))